// import word from './es6-test';

// export default () => {
//     console.log(word);
// };

// 使用rollup-plugin-json，令rollup从json文件中读取数据
import { version } from '../package.json';

export default function () {
  console.log('version ' + version);
}