module.exports = class Queue {
    constructor () {
        this.jobs = [];
        this.running = false;
    }
     getLength () {
        return this.jobs.length;
    }

     push (el) {
        this.jobs.push(el);
    }

     stop () {
        this.running = false;
    }

     start () {
        this.running = true;

        const next = (data) => {
           
            const job = this.jobs.shift();
            if (!job) {
                return;
            }


            if (this.running) {
                job(next, data);    
            }
        }


        if (this.running && this.jobs.length > 0) {
            next();
        }  
    }
}