const foo = {
    isTest: 'fool',
    say () {
        console.log(this)
        console.log(`foo isTest: ${this.isTest}`);
        // console.log('I am normal');
    }
}

const bar = {
    isTest: 'bar',
    say () {
        console.log(this)
        console.log(`bar isTest: ${this.isTest}`);
        // console.log('I use double colons');
    }
}

// 为foo.say函数绑定bar对象
const fooToBar = bar :: foo.say;
// console.log(fooToBar)
console.log(fooToBar());
// console.log(foo.say.call(bar));