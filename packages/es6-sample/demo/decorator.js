/**
 * 类装饰器实例1
 */
@testable
class Test {}

function testable (target) {
    target.isTest = true;
    console.log(target)
}

/**
 * 类装饰器实例2
 * 1、可传递多个参数
 * 2、添加实例属性 -- target.prototype.xxx = xxx
 * 3、添加静态属性 -- target.xxx = xxx
 */
function mixins (isTest, ...list) {
    return function (target) {
        target.isTest = isTest;
        Object.assign(target.prototype, ...list)
    }
}

const Foo = {
    say: function () {
        console.log('hello');
    },
    isTest: false
};

@mixins(true, Foo)
class MyTest {}

const myT = new MyTest();
console.log(myT);
myT.say();
console.log(`实例上的isTest（实例属性）：${myT.isTest},`, `类上的isTest（静态属性）：${MyTest.isTest}`);