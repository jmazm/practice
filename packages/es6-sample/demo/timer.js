// import Timer from 'timer.js';
// console.log(Timer);
// const timer = new Timer();
// timer.start(5).on('end', () => {
//     console.log(`end: 1`)
// })

const Timer = require('timer.js');
const Queue = require('./queue.js');
const timer = new Timer();
const timer2 = new Timer();
const q = new Queue();

const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

arr.forEach((item, index) => {
    q.push(cb => {
        console.time(`item: ${item}`)
        console.log(`item: ${item}`);
        cb();
    });

    if (index % 2 === 1) {
        q.push(cb => {
            timer.start(5).on('end', () => {
                // console.log(`timeEnd: ${index}`);
                console.timeEnd(`item: ${item}`)
                cb();
            });
        })
    }
});


q.start();

// const timer = new Timer();
timer2.start(5).on('end', () => {
    console.log(`end: 1`)
})