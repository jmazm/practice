"use strict";

var foo = {
  isTest: 'fool',
  say: function say() {
    console.log(this);
    console.log("foo isTest: ".concat(this.isTest)); // console.log('I am normal');
  }
};
var bar = {
  isTest: 'bar',
  say: function say() {
    console.log(this);
    console.log("bar isTest: ".concat(this.isTest)); // console.log('I use double colons');
  }
}; // 为foo.say函数绑定bar对象

var fooToBar = foo.say.bind(bar); // console.log(fooToBar)

console.log(fooToBar()); // console.log(foo.say.call(bar));
