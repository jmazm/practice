"use strict";

var _class, _dec, _class2;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * 类装饰器实例1
 */
var Test = testable(_class = function Test() {
  _classCallCheck(this, Test);
}) || _class;

function testable(target) {
  target.isTest = true;
  console.log(target);
}
/**
 * 类装饰器实例2
 * 1、可传递多个参数
 * 2、添加实例属性 -- target.prototype.xxx = xxx
 * 3、添加静态属性 -- target.xxx = xxx
 */


function mixins(isTest) {
  for (var _len = arguments.length, list = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    list[_key - 1] = arguments[_key];
  }

  return function (target) {
    target.isTest = isTest;
    Object.assign.apply(Object, [target.prototype].concat(list));
  };
}

var Foo = {
  say: function say() {
    console.log('hello');
  },
  isTest: false
};
var MyTest = (_dec = mixins(true, Foo), _dec(_class2 = function MyTest() {
  _classCallCheck(this, MyTest);
}) || _class2);
var myT = new MyTest();
console.log(myT);
myT.say();
console.log("\u5B9E\u4F8B\u4E0A\u7684isTest\uFF08\u5B9E\u4F8B\u5C5E\u6027\uFF09\uFF1A".concat(myT.isTest, ","), "\u7C7B\u4E0A\u7684isTest\uFF08\u9759\u6001\u5C5E\u6027\uFF09\uFF1A".concat(MyTest.isTest));
