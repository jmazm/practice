const puppeteer = require('puppeteer');
const path = require('path');
const fs = require('fs');

async function run () {
    const browser = await puppeteer.launch({
        headless: false // 启动GUI界面，进行可视化调试
    });
    const page = await browser.newPage();
    const dir = path.resolve(__dirname, 'screenshots');
    !fs.existsSync(dir) && fs.mkdirSync(dir);

    await page.goto('https://www.baidu.com');
    await page.screenshot({path: 'screenshots/baidu.png'});

    browser.close();
}

run();