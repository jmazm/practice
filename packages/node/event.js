const EventEmitter = require('events');

const emitter = new EventEmitter();

emitter.once('log', () => console.log('hello, 11'))

const listeners = emitter.rawListeners('log');
const logFnWrapper = listeners[0];

// 打印hello,11，不会解绑once事件
logFnWrapper.listener();

// 打印hello，11，移除事件监听器
logFnWrapper();


emitter.on('log', () => console.log('log persistently'));
const newListeners = emitter.rawListeners('log');
newListeners[0]();
emitter.emit('log');