const crypto = require('crypto');


const hash1 = crypto.createHash('sha1');
hash1.update('HEllo World');
console.log(hash1.digest('hex'));


const hash2 = crypto.createHash('sha1');
hash2.update('hhhhh');
console.log(hash2.digest('hex'));


const hash3 = crypto.createHash('sha1');
hash3.update('HEllo World');
console.log(hash3.digest('hex'));