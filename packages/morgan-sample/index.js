var express = require('express');
var app = express();
var morgan = require('morgan');
var fs = require('fs');
var path = require('path');
var FileStreamRotator = require('file-stream-rotator')

var logDirectory = path.join(__dirname, 'log')

// ensure log directory exists
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory)

// create a rotating write stream
var accessLogStream = FileStreamRotator.getStream({
    date_format: 'YYYYMMDD',
    filename: path.join(logDirectory, 'access-%DATE%.log'),
    frequency: 'daily',
    verbose: false
})

app.use(
    morgan('combined', {
            stream: accessLogStream // 通过配置stream这个字段来实现将日志落地，即打印到本地文件中
        }
    )
);

app.get('/', function (req, res) {
    res.send('hello, world!')
})

app.listen(3000);