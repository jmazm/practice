const fs = require('fs');
const path = require('path');

const getDir = (dir) => {
	const data = fs.readdirSync(dir);
	console.error('dir', data);
	return data;
}

const pagesTpl = [
	'plop/templates/pages/index.hbs',
	'plop/templates/pages/styles.hbs'
]

const modulesTpl = [
	'plop/templates/modules/index.hbs',
	'plop/templates/modules/configureStore.hbs',
	'plop/templates/modules/reducers.hbs',
	'plop/templates/modules/store.hbs'
]



module.exports = function (plop) {
	plop.setGenerator('modules', {
		description: 'modules logic',
		prompts: [{
			type: 'input',
			name: 'module',
			message: 'create a new module',
			validate: function (value) {
				if ((/.+/).test(value)) { return true; }
				return 'module name is required';
			}
		}],
		actions: [
			{
				type: 'addMany',
				base: 'plop/templates/pages',
				destination: 'plop/src/modules/{{module}}/pages',
				templateFiles: 'plop/templates/pages/**',
				transform: (content, data) => {
					return content.replace(/{cName}/, `${data.module}`)
				}
			},
			{
				type: 'addMany',
				base: 'plop/templates/modules',
				destination: 'plop/src/modules/{{module}}',
				templateFiles: 'plop/templates/modules/**',
			}
		]
	});

	plop.setGenerator('pages', {
		description: 'pages logic',
		prompts: [{
			type: 'list',
			name: 'module',
			message: 'choose a module',
			choices: () => {
				return getDir(path.resolve(process.cwd(), 'plop/src/modules'))
			}
		}, {
			type: 'input',
			name: 'page',
			message: 'create a new page',
			validate: function (value) {
				if ((/.+/).test(value)) { return true; }
				return 'page name is required';
			}
		}],
		actions: [
			{
				type: 'addMany',
				base: 'plop/templates/pages',
				destination: 'plop/src/modules/{{module}}/pages/{{page}}',
				templateFiles: 'plop/templates/pages/**',
				transform: (content, data) => {
					return content.replace(/{cName}/, `${data.module}-${data.page}`)
				}
			},
		]
	});
}