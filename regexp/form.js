const strategy = {
  isEmpty: (val) => {
    return `${val}`.trim().length === 0;
  },
  isPhone: (val) => {
    return /^1[3|4|5|8]\d{9}$/.test(val);
  },
  isEmail: (val) => {
    // 0-9a-zA-Z_@

    return /^([\w\-\.])+\@([\w\-\.])+\.([A-Za-z]{2,6})$/.test(val)
  },
  isNumber: (val) => {
    return !isNaN(Number(val));
  }
}

console.error('--- isEmpty ---');
console.error('15626168865 ', strategy.isEmpty('15626168865 '))
console.error('', strategy.isEmpty(''))
console.error(' ', strategy.isEmpty(' '))
console.error(`    `, strategy.isEmpty(`    `))
console.error('\s\s', strategy.isEmpty('\s\s'))
console.error('\s\s', strategy.isEmpty('\s\s'))


console.error('--- isPhone ---');
console.error('15626168865', strategy.isPhone('15626168865'))
console.error('1562616886511', strategy.isPhone('1562616886511'))
console.error('15626168865 ', strategy.isPhone('15626168865 '))
console.error('15626dddd168865 ', strategy.isPhone('15626dddd168865 '))

console.error('--- isEmail ---');
console.error('15626168865@qq.com', strategy.isEmail('15626168865@qq.com'))
console.error('15626168865@', strategy.isEmail('15626168865@'))
console.error('15626168865@qq', strategy.isEmail('15626168865@qq'))
console.error('15626168865山东航空@qq.com', strategy.isEmail('15626168865山东航空@qq.com'))


console.error('--- isNumber ---');
console.error('15626168865@qq.com', strategy.isNumber('15626168865@qq.com'))
console.error('15626168865', strategy.isNumber('15626168865'))