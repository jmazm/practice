import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import App from './pages/index';
import './styles/main.less';
// import '../../vendors/lib-flexible/flexible.debug.js';
// import '../../vendors/lib-flexible/flexible_css.debug.js';
import { store, history } from './store';

// if(process.env.NODE_ENV !== 'production') {
// 	require('@/common/helpers/vconsole.js');
// }

const MOUNT_NODE = document.getElementById('root');

function render(Component) {
  ReactDOM.render(
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <Component />
      </ConnectedRouter>
    </Provider>,
    MOUNT_NODE,
  );
}

render(App);

// Install ServiceWorker and AppCache in the end since
// it's not most important operation and if main code fails,
// we do not want it installed
if (process.env.NODE_ENV === 'production') {
  require('offline-plugin/runtime').install(); // eslint-disable-line global-require
}

// hot load
if (module.hot) {
  module.hot.accept();
  // module.hot.accept('./pages/index', () => {
  // 	render(Mobile);
  // })
}
