import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router';

import './style.less';
interface OwnerProps {}

interface StateProps {}

// 应用主题色
window.EN5_theme_manager && window.EN5_theme_manager.applyNewTheme();

class App extends React.Component<RouteComponentProps & OwnerProps, StateProps> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="share-root-container">
      </div>
    );
  }
}

const mapStateToProps = state => ({});

export default withRouter(connect(mapStateToProps)(App));
