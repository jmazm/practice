var path = require('path');

module.exports = {
  mode: 'development',
  entry: {
    common: path.resolve(process.cwd(), 'webpack/common/index.js'),
    // b: path.resolve(process.cwd(), 'webpack/b.js'),
  },
  output: {
    path: path.resolve(process.cwd(), 'webpack/dist'),
    filename: '[name].bundle.js'
  }
};