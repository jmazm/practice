/**
 * 打包生成的代码是一个IIFE（立即执行函数）
 * 
 * 一个函数就是一个模块，原因：浏览器本身不支持模块化，webpack用函数作用域来hack模块化的效果
 */

(function(modules) {
	/* 省略函数内容 */
})({
	'./webpack/common/bar.js': function(module, exports, __webpack_require__) {
		/* 模块bar.js的代码 */
	},
	'./webpack/common/index.js': function(module, exports, __webpack_require__) {
		/* 模块index.js的代码 */
	}
});

(function(modules) {
	/* 省略函数内容 */

	// 1、模块缓存对象
	var installedModules = {};

	// 2、webpack自己实现的require函数
	function __webpack_require__(moduleId) {
		// 3、判断是否已缓存模块
		if (installedModules[moduleId]) {
			return installedModules[moduleId].exports;
		}

		// 4、缓存模块
		var module = (installedModules[moduleId] = {
			i: moduleId,
			l: false,
			exports: {}
		});

		// 5、调用模块函数
		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

		// 6、标记模块为已加载
		module.l = true;

		// 7、返回module.exports
		return module.exports;
  }
  
  	// 加载入口文件
    return __webpack_require__(__webpack_require__.s = "./webpack/common/index.js");
})({
	'./webpack/common/bar.js': function(module, exports, __webpack_require__) {
		/* 模块bar.js的代码 */
	},
	'./webpack/common/index.js': function(module, exports, __webpack_require__) {
		/* 模块index.js的代码 */
	}
});



