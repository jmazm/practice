const toolTipFactory = (function (){
  const toolTipPool = [];

  return {
    create: function () {
      if (toolTipPool.length === 0) {
        const div = document.createElement('div');
        document.body.appendChild(div);
        return div;
      } else {
        return toolTipPool.shift();
      }
    },
    recover: function (toolTipDom) {
      return toolTipPool.push(toolTipDom);
    }
  }
})()


const arr = [];
let tips = ['a', 'b'];

for (const t of tips) {
  const tip = toolTipFactory.create();
  tip.innerHTML = t;
  arr.push(tip);
}

for (const t of arr) {
  toolTipFactory.recover(t);
}

tips = [ 'A', 'B', 'C', 'D', 'E', 'F' ]

for (const t of tips) {
  const tip = toolTipFactory.create();
  tip.innerHTML = t;
}