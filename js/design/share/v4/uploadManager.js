const uploadManager = (function () {
  const uploadDatabase = {};

  return {
    add: function (id, uploadType, fileName, fileSize) {
      const flyWeightObj = UploadFactory.create(uploadType);

      const dom = document.createElement('div');
      dom.innerHTML = `
        <span>文件名：${fileName}，文件大小：${fileSize}
        <button class="delFile">删除</button>
      `;

      dom.querySelector('.delFile').onclick = function () {
        console.log(id)
        flyWeightObj.delFile(id);
      };

      document.body.appendChild(dom);
      uploadDatabase[id] = {
        fileName,
        fileSize,
        dom,
      };
    },
    setExternalState: function (id, flyWeightObj) {
      const uploadData = uploadDatabase[id];
      for (const i in uploadData) {
        flyWeightObj[i] = uploadData[i];
      }
    },
  };
})();
