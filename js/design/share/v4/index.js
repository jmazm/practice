let id = 0;

window.startUpload = function (uploadType, files) {
  for (const file of files) {
    uploadManager.add(++id, uploadType, file.fileName, file.fileSize);
  }
};

startUpload('plugin', [
  {
    fileName: '1.txt',
    fileSize: 1000,
  },
  {
    fileName: '2.html',
    fileSize: 3000,
  },
  {
    fileName: '3.txt',
    fileSize: 5000,
  },
]);
startUpload('flash', [
  {
    fileName: '4.txt',
    fileSize: 1000,
  },
  {
    fileName: '5.html',
    fileSize: 3000,
  },
  {
    fileName: '6.txt',
    fileSize: 5000,
  },
]);
