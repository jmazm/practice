const Upload = function (uploadType, fileName, fileSize) {
  this.uploadType = uploadType;
  this.fileName = fileName;
  this.fileSize = fileSize;
  this.wrapper = null;
};

Upload.prototype.delFile = function () {
  if (
    this.fileSize < 3000 ||
    window.confirm(`确定要删除该文件？${this.fileName}`)
  ) {
    return this.wrapper.parentNode.removeChild(this.wrapper);
  }
};

Upload.prototype.init = function (id) {
  const that = this;
  this.id = id;
  this.wrapper = document.createElement('div');
  this.wrapper.innerHTML = `
    <span>文件名：${this.fileName}，文件大小：${this.fileSize}
    <button class="delFile">删除</button>
  `;

  this.wrapper.querySelector('.delFile').onclick = function () {
    that.delFile();
  };

  document.body.appendChild(this.wrapper);
};
