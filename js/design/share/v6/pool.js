const objectPoolFactory = function (createObjFn) {
  const pools = [];
  return {
    create() {
      const obj =
        pools.length === 0 ? createObjFn.apply(this, arguments) : pools.shift();
      return obj;
    },
    recover(obj) {
      pools.push(obj);
    },
  };
};

var iframeFactory = objectPoolFactory(function () {
  var iframe = document.createElement('iframe');
  document.body.appendChild(iframe);
  iframe.onload = function () {
    iframe.onload = null; // 防止 iframe 重复加载的 bug
    iframeFactory.recover(iframe); // iframe 加载完成之后回收节点
  };
  return iframe;
});
var iframe1 = iframeFactory.create();
iframe1.src = 'http:// baidu.com';
var iframe2 = iframeFactory.create();
iframe2.src = 'http:// QQ.com';
setTimeout(function () {
  var iframe3 = iframeFactory.create();
  iframe3.src = 'http:// 163.com';
}, 3000);
