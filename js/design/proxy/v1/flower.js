/**
 * 无代理
 */

// const Flower = function () {}

// const A = {
//   recieveFlower (flower) {
//     console.log(`收到花${flower}`)
//   }
// }

// const xiaoming = {
//   sendFlower (target) {
//     const flower = new Flower();
//     target.recieveFlower(flower);
//   }
// }

// xiaoming.sendFlower(A);



/**
 * 引入代理B，小明通过B给A送花
 */

// const Flower = function () {}

// const xiaoming = {
//   sendFlower (target) {
//     const flower = new Flower();
//     target.recieveFlower(flower);
//   }
// }

// const B = {
//   recieveFlower (flower) {
//     A.recieveFlower(flower);
//   }
// }

// const A = {
//   recieveFlower (flower) {
//     console.log(`收到花${flower}`)
//   }
// }

// xiaoming.sendFlower(B);


/**
 * 
 */

const Flower = function (){}

const xiaoming = {
  sendFlower (target) {
    target.recieveFlower();
  }
}

const B = {
  recieveFlower (){
    A.listenGoodMood(() => {
      const flower = new Flower();
      A.recieveFlower(flower);
    })
  }
}

const A = {
  recieveFlower (flower) {
    console.log(`收到花${flower}`)
  },
  listenGoodMood (fn) {
    setTimeout(function () {
      fn();
    }, 10000)
  }
}

xiaoming.sendFlower(B);