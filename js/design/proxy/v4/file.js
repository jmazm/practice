const synchronousFile = function (id) {
  console.log(`开始同步文件，ID为${id}`);
};

const proxySynchronousFile = (function () {
  // 保存一段时间内需要同步的ID
  const cache = [];
  let timer = null;

  return function (id) {
    cache.push(id);
    // 保证不会覆盖已经启动的定时器
    if (timer) {
      return;
    }

    // 2s后向本体发送需要同步的 ID 集合
    timer = setTimeout(function () {
      synchronousFile(cache.join(','));
      // 清空定时器
      clearTimeout(timer);
      timer = null;
      // 清空缓存集合
      cache.length = 0;
    }, 2000);
  };
})();

const checkboxes = [...document.getElementsByTagName('input')];

for (const c of checkboxes) {
  c.onclick = function () {
    if (this.checked) {
      proxySynchronousFile(this.id);
    }
  };
}
