const myImg = (function (){
  const imgNode = document.createElement('img');
  document.body.appendChild(imgNode);

  return {
    setSrc (src) {
      imgNode.src = src;
    }
  }
})()

const proxyImage = (function(){
  const img = new Image();
  img.onload = function () {
    myImg.setSrc(this.src);
  }
  return {
    setSrc (src) {
      myImg.setSrc('https://www.google.com/url?sa=i&url=https%3A%2F%2Fgiphy.com%2Fexplore%2Floading&psig=AOvVaw1x8F-D-Cy4dSexZQZ8rEbJ&ust=1614392305620000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCJCl45G-hu8CFQAAAAAdAAAAABAD')
      img.src = src;
    }
  }
})()

proxyImage.setSrc('https://www.brickaffe.com/wp-content/uploads/2020/07/IMG_9897-1024x683.jpg')
