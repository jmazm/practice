const FSM = {
  on: {
    buttonWasPressed: function () {
      console.log('关灯')
      this.button.innerHTML = '下一次按我是开灯'
      this.currentState = this.offState;
    },
  },
  off: {
    buttonWasPressed: function () {
      console.log('开灯')
      this.button.innerHTML = '下一次按我是关灯'; 
      this.currentState = this.onState;
    },
  },
};


const delegate = function (client, delegation) {
  return {
    buttonWasPressed: function () {
      // 将客户的操作委托给 delegation 对象
      return delegation.buttonWasPressed.apply(client, arguments);
    }
  }
}

const Light = function () {
  this.onState = delegate(this, FSM.on);
  this.offState = delegate(this, FSM.off);
  this.currentState = this.offState;
  this.button = null;
}

Light.prototype.init = function () {
  const btn = document.createElement('button');
  const self = this;
  btn.innerHTML = '开关';
  this.button = document.body.appendChild(btn);

  // 设置当前的状态
  this.currentState = this.offState;

  this.button.onclick = function () {
    self.currentState.buttonWasPressed();
  };
}

var light = new Light();
light.init();