const Light = function () {
  this.state = 'off';
  this.button = null;
}

Light.prototype.init = function () {
  const btn = document.createElement('button');
  const self = this;
  btn.innerHTML = '开关';
  this.button = document.body.appendChild(btn);
  this.button.onclick = function () {
    self.buttonWasPressed();
  }
}

Light.prototype.buttonWasPressed = function () {
  if (this.state === 'off') {
    console.log('开灯');
    this.state = 'on';
  } else if (this.state === 'on') {
    console.log('关灯');
    this.state = 'off';
  }
}

const light = new Light();
light.init();