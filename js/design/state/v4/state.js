const SignState = StateFactory({
  clickHandler1: function () {
    console.log('扫描中，点击无效...');
  },
  clickHandler2: function () {
    console.log('文件正在上传中，无法删除');
  },
});

const UploadingState = StateFactory({
  clickHandler1: function () {
    this.uploadObj.pause();
  },
  clickHandler2: function () {
    console.log('文件正在上传中，无法删除');
  },
});

const PauseState = StateFactory({
  clickHandler1: function () {
    this.uploadObj.uploading();
  },
  clickHandler2: function () {
    this.uploadObj.del();
  },
});


const DoneState = StateFactory({
  clickHandler1: function () {
    console.log('文件已经上传完毕，点击无效')
  },
  clickHandler2: function () {
    this.uploadObj.del();
  },
})

const ErrorState = StateFactory({
  clickHandler1: function () {
    console.log('文件上传失败，点击无效')
  },
  clickHandler2: function () {
    this.uploadObj.del();
  },
})