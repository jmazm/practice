const Upload = function (fileName) {
  this.plugin = plugin;
  this.fileName = fileName;
  this.wrapper = null;
  this.btn1 = null;
  this.btn2 = null;
  this.signState = new SignState(this);
  this.uploadingState = new UploadingState(this);
  this.pauseState = new UploadingState(this);
  this.doneState = new DoneState(this);
  this.errorState = new ErrorState(this);

  // 设置当前状态
  this.currentState = this.signState;
};

Upload.prototype.sign = function () {
  this.plugin.sign();
  this.currentState = this.signState;
};

Upload.prototype.uploading = function () {
  this.btn1.innerHTML = '正在上传中，点击暂停';
  this.plugin.uploading();
  this.currentState = this.uploadingState;
};

Upload.prototype.pause = function () {
  this.btn1.innerHTML = '已暂停，点击继续上传';
  this.plugin.pause();
  this.currentState = this.pauseState;
};

Upload.prototype.done = function () {
  this.btn1.innerHTML = '上传完成';
  this.plugin.done();
  this.currentState = this.doneState;
};

Upload.prototype.error = function () {
  this.btn1.innerHTML = '上传失败';
  this.currentState = this.errorState;
}

Upload.prototype.del = function () {
  this.plugin.del();
  this.wrapper.parentNode.removeChild(this.wrapper);
}

Upload.prototype.bindEvent = function () {
  const self = this;

  this.btn1.onclick = function () {
    self.currentState.clickHandler1();
  }

  this.btn2.onclick = function () {
    self.currentState.clickHandler2();
  }
};

Upload.prototype.init = function () {
  this.wrapper = document.createElement('div');
  this.wrapper.innerHTML = `
  <span>文件名称：${this.fileName}</span>
  <button data-action="btn1">扫描中</button>
  <button data-action="btn2">删除</button>
  `;

  document.body.appendChild(this.wrapper);
  this.btn1 = this.wrapper.querySelector('[data-action="btn1"]');
  this.btn2 = this.wrapper.querySelector('[data-action="btn2"]');
  this.bindEvent();
};
