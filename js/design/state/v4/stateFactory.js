var StateFactory = (function () {
  const State = function () {};

  State.prototype.clickHandler1 = function () {
    throw new Error('子类必须重写父类的 clickHandle1 方法');
  };

  State.prototype.clickHandler2 = function () {
    throw new Error('子类必须重写父类的 clickHandle2 方法');
  };

  return function (params) {
    const F = function (uploadObj) {
      this.uploadObj = uploadObj;
    };

    F.prototype = new State();

    for (const p in params) {
      F.prototype[p] = params[p];
    }

    return F;
  };
})();
