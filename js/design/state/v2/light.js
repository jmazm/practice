// OffLightState：
var OffLightState = function (light) {
  this.light = light;
};
OffLightState.prototype.buttonWasPressed = function () {
  console.log('弱光'); // offLightState 对应的行为
  this.light.setState(this.light.weakLightState); // 切换状态到 weakLightState
};
// WeakLightState：
var WeakLightState = function (light) {
  this.light = light;
};
WeakLightState.prototype.buttonWasPressed = function () {
  console.log('强光'); // weakLightState 对应的行为
  this.light.setState(this.light.strongLightState); // 切换状态到 strongLightState
};
// StrongLightState：
var StrongLightState = function (light) {
  this.light = light;
};
StrongLightState.prototype.buttonWasPressed = function () {
  console.log('关灯'); // strongLightState 对应的行为
  this.light.setState(this.light.offLightState); // 切换状态到 offLightState
};

const Light = function () {
  this.offLightState = new OffLightState(this);
  this.weakLightState = new WeakLightState(this);
  this.strongLightState = new StrongLightState(this);
  this.button = null;
  this.currentState = null;
};

Light.prototype.init = function () {
  const btn = document.createElement('button');
  const self = this;
  btn.innerHTML = '开关';
  this.button = document.body.appendChild(btn);

  // 设置当前的状态
  this.currentState = this.offLightState;

  this.button.onclick = function () {
    self.currentState.buttonWasPressed();
  };
};

Light.prototype.setState = function (state) {
  this.currentState = state;
};

const light = new Light();
light.init();
