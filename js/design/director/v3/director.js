const playerDirector = (function () {
  const players = {}; // 保存所有玩家
  const operations = {}; // 中介者可以执行的操作

  /**
   * 新增一个玩家
   * @param {*} player
   */
  operations.addPlayer = function (player) {
    // 玩家的队伍颜色
    const teamColor = player.teamColor;
    // 如果该颜色的玩家还没有成立队伍，则成立一个队伍
    players[teamColor] = players[teamColor] || [];
    // 添加玩家进入队伍
    players[teamColor].push(player);
  };

  /**
   * 移除一个玩家
   * @param {*} player
   */
  operations.removePlayer = function (player) {
    const teamColor = player.teamColor;
    const teamPlayers = players[teamColor] || [];
    const len = teamPlayers.length;

    for (let i = len - 1; i >= 0; i--) {
      if (teamPlayers[i] === player) {
        teamPlayers.splice(i, 1);
      }
    }
  };

  /**
   * 玩家换队
   * @param {*} player
   * @param {*} newTeamColor
   */
  operations.changeTeam = function (player, newTeamColor) {
    operations.removePlayer(player);
    player.teamColor = newTeamColor;
    operations.addPlayer(player);
  };

  /**
   * 玩家死亡
   * @param {*} player
   */
  operations.playerDead = function (player) {
    const teamColor = player.teamColor;
    // 玩家所在队伍
    const teamPlayers = players[teamColor];

    let all_dead = true;

    for (const player of teamPlayers) {
      // 如果有一个队友没有死亡，则游戏还没结束
      if (player.state !== 'dead') {
        all_dead = false;
        break;
      }
    }

    // 队友全死亡
    if (all_dead) {
      // 通知所有队友玩家游戏失败
      for (const player of teamPlayers) {
        player.lose();
      }

      // 通知所有敌人游戏胜利
     for (const color in players) {
       if (color !== teamColor) {
         // 其他玩家队伍，胜利
         const otherTeamPlayers = players[color];
         for (const player of otherTeamPlayers) {
           player.win();
         }
       }
     }
    }
  };

  const recieveMessage = function () {
    const message = Array.prototype.shift.call(arguments);
    operations[message].apply(this, arguments);
  }

  return {
    recieveMessage
  }
})();
