function Player(name, teamColor) {
  this.partners = []; // 队友列表
  this.enemies = []; // 敌人列表
  this.state = 'live'; // 玩家状态
  this.name = name; // 角色名字
  this.teamColor = teamColor; // 队伍颜色;
}

/**
 * 玩家团队胜利
 */
Player.prototype.win = function () {
  console.log(`winner: ${this.name}`);
};

/**
 * 玩家团队失败
 */
Player.prototype.lose = function () {
  console.log(`loser: ${this.name}`);
};

/**
 * 玩家死亡
 */
Player.prototype.die = function () {
  // 设置玩家状态为死亡
  this.state = 'dead';
  playerDirector.recieveMessage('playerDead', this);
};

Player.prototype.remove = function () {
  playerDirector.recieveMessage('removePlayer', this);
};

Player.prototype.changeTeam = function (color) {
  playerDirector.recieveMessage('changeTeam', this, color);
};

const players = [];

const playerFactory = function (name, teamColor) {
  // 创建新玩家
  const newPlayer = new Player(name, teamColor);

  playerDirector.recieveMessage('addPlayer', newPlayer);

  return newPlayer;
};
