function Player(name, teamColor) {
  this.partners = []; // 队友列表
  this.enemies = []; // 敌人列表
  this.state = 'live'; // 玩家状态
  this.name = name; // 角色名字
  this.teamColor = teamColor; // 队伍颜色;
}

/**
 * 玩家团队胜利
 */
Player.prototype.win = function () {
  console.log(`winner: ${this.name}`);
};

/**
 * 玩家团队失败
 */
Player.prototype.lose = function () {
  console.log(`loser: ${this.name}`);
};

/**
 * 玩家死亡
 */
Player.prototype.die = function () {
  // 设置玩家状态为死亡
  this.state = 'dead';

  let all_dead = true;
  const partnerLen = this.partners.length;

  for (let i = 0; i < partnerLen; i++) {
    const partner = this.partners[i];
    // 如果有一个队友没有死亡，则游戏还没结束
    if (partner.state !== 'dead') {
      all_dead = false;
      break;
    }
  }

  // 队友全死亡
  if (all_dead) {
    this.lose();
    // 通知所有队友玩家游戏失败
    for (let i = 0; i < partnerLen; i++) {
      const partner = this.partners[i];
      partner.lose();
    }

    // 通知所有敌人游戏胜利
    const enenmyLen = this.enemies.length;
    for (let i = 0; i < enenmyLen; i++) {
      const enemy = this.enemies[i];
      enemy.win();
    }
  }
};

const players = [];

const playerFactory = function (name, teamColor) {
  // 创建新玩家
  const newPlayer = new Player(name, teamColor);

  const len = players.length;

  // 通知所有玩家，有新角色加入
  for (let i = 0; i < len; i++) {
    const player = players[i];
    // 同一队玩家，相互添加到队友列表
    if (player.teamColor === newPlayer.teamColor) {
      player.partners.push(newPlayer);
      newPlayer.partners.push(player);
    } else { // 不是同一队玩家，相互添加到敌人列表
      player.enemies.push(newPlayer);
      newPlayer.enemies.push(player);
    }
  }

  players.push(newPlayer);

  return newPlayer;
}