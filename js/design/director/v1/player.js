function Player(name) {
  this.name = name;
  this.enemy = null;
}

Player.prototype.win = function () {
  console.log(`${this.name} won`);
}

Player.prototype.lose = function () {
  console.log(`${this.name} lost`);
}

Player.prototype.die = function () {
  this.lose();
  this.enemy.win();
}
