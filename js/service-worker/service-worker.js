// 添加缓存
self.addEventListener('install', (event) => {
    // waitUntil()接收一个promise对象，直到这个promise对象返回成功后，才会继续进行service worker后续的运行
    // 等到service worker 被激活/被install之前，我们可以先做一些事情 -》 缓存
    // 其实这里是阻塞了一下
    event.waitUntil(
        // caches.open()返回一个promise对象
        // caches其实是CacheStorage对象
        caches.open('app-v1')
            .then((cache) => {
                console.log('open cache');
                cache.addAll([
                    // 文件的路径？？？？
                    './index.js',
                    './index.css',
                    './index.html'
                ]);
            })
    )
});

// 使用缓存
// fetch事件 - 拦截请求、伪造响应
// 拦截请求：相当于对页面上所有的请求做了一个代理，然后在浏览器的service worker中，对这个请求的返回做一个伪造和改变
self.addEventListener('fetch', (event) => {
    event.respondWith(
        caches.match(event.request).then((res) => {
            if(res) {
                return res;
            } else {
                // 没有，通过fetch方法向网络发起请求
                /*
                fetch(url).then(res => {
                    if(res) {
                        // 对于新请求回来的资源，存储到cacheStorage中
                    } else {
                        // 用户提示
                    }
                })
                */
            }
        })
    );
});