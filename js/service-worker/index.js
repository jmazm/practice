if(navigator.serviceWorker) {
    navigator.serviceWorker.register('./service-worker.js', {
        scope: './'
    });
} else {
    console.warn('Service Worker is not supported!')
}