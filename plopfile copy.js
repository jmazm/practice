const fs = require('fs');
const path = require('path');

const getDir = (dir) => {
	const data = fs.readdirSync(dir);
	console.error('dir', data);
	return data;
}

// module.exports = function (plop) {
//   plop.setGenerator('controller', {
// 		description: 'application controller logic',
// 		prompts: [{
// 			type: 'list',
// 			choices: function (data) {
// 				const dir = getDir(path.resolve(process.cwd()));
// 				return dir
// 			},
// 			name: 'module',
// 			message: 'choose module please'
// 		},
// 		{
// 			type: 'input',
// 			name: 'page',
// 			message: 'input page name please'
// 		}],
// 		actions: [{
// 			type: 'add',
// 			path: 'plop/src/{{module}}/{{page}}.js',
// 			templateFile: 'plop/templates/controller.hbs'
// 		}]
// 	});



module.exports = function (plop) {
	plop.setGenerator('modules', {
		description: 'modules logic',
		prompts: [{
			type: 'input',
			name: 'module',
			message: 'create a new module'
		}],
		actions: [
			{
			type: 'add',
			path: 'plop/src/modules/{{module}}/pages/index.js',
			templateFile: 'plop/templates/pages/index.hbs'
		},{
			type: 'add',
			path: 'plop/src/modules/{{module}}/pages/styles.less',
			templateFile: 'plop/templates/pages/styles.hbs'
		},
		{
			type: 'add',
			path: 'plop/src/modules/{{module}}/index.js',
			templateFile: 'plop/templates/modules/index.hbs'
		},
		{
			type: 'add',
			path: 'plop/src/modules/{{module}}/configureStore.js',
			templateFile: 'plop/templates/modules/configureStore.hbs'
		},
		{
			type: 'add',
			path: 'plop/src/modules/{{module}}/reducers.js',
			templateFile: 'plop/templates/modules/reducers.hbs'
		},
		{
			type: 'add',
			path: 'plop/src/modules/{{module}}/store.js',
			templateFile: 'plop/templates/modules/store.hbs'

		}
	]
	});

	plop.setGenerator('pages', {
		description: 'pages logic',
		prompts: [{
			type: 'list',
			name: 'module',
			message: 'choose a module',
			choices: () => {
				return getDir(path.resolve(process.cwd(), 'plop/src/modules'))
			}
		}, {
			type: 'input',
			name: 'page',
			message: 'create a new page'
		}],
		actions: data => {
			const {} = data;
			console.error(data);
			return [{
							type: 'add',
							path: 'plop/src/modules/{{module}}/pages/{{page}}/index.js',
							templateFile: 'plop/templates/pages/index.hbs'
						},{
							type: 'add',
							path: 'plop/src/modules/{{module}}/pages/{{page}}/styles.less',
							templateFile: 'plop/templates/pages/styles.hbs'
						}]
		}
	});
}