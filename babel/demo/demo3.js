const markTwain = require('mark-twain');
const JsonML = require('jsonml.js/lib/utils');

function getCode(tree) {
  let code;
  const find = (node) => {
    if (code) return;
    if (!JsonML.isElement(node)) return;
    if (JsonML.getTagName(node) !== 'pre') {
      JsonML.getChildren(node).forEach(find);
      return;
    }
    code = JsonML.getChildren(JsonML.getChildren(node)[0] || '')[0] || '';
  };
  find(tree);
  return code;
}

export default ({ types: t }) => {
  return {
    visitor: {
      Program(path) {
        const importReact = t.ImportDeclaration(
          [t.importDefaultSpecifier(t.Identifier('React'))],
          t.StringLiteral('react')
        );

        path.unshiftContainer('body', importReact);
      },
      CallExpression(path) {
        if (
          path.node.callee.object &&
          path.node.callee.object.name === 'ReactDOM' &&
          path.node.callee.property.name === 'render'
        ) {
          const app = t.VariableDeclaration('const', [
            t.VariableDeclarator(t.Identifier('__Demo'), path.node.arguments[0])
          ]);

          path.scope.registerDeclaration(path.replaceWith(app)[0]);
          const exportDefault = t.ExportDefaultDeclaration(t.Identifier('__Demo'));

          path.insertAfter(exportDefault);
          path.insertAfter(app);
          path.remove();
        }
      },
      ImportDeclaration(path) {
        console.log(path.node)
      }
    },
  };
};
