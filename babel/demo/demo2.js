import * as babylon from "babylon";
import * as babelTypes from "babel-types";
import traverse from "babel-traverse";
import generate from "babel-generator";
import template from "babel-template";

export default function ({types}) {
  return {
    visitor: {
      BinaryExpression (path) {
        console.log('22', path.node.operator, path.node)
        if (path.node.operator !== '===') {
          return;
        }

        path.node.left = babelTypes.identifier('a1');
        path.node.right = babelTypes.identifier('b1');
      }
    }
  }
}