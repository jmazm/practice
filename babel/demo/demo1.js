import * as babylon from "babylon";
import * as babelTypes from "babel-types";
import traverse from "babel-traverse";
import generate from "babel-generator";
import template from "babel-template";

const buildRequire = template(`
  var IMPORT_NAME = require(SOURCE);
`);

const code = `n * n`;

const ast = babylon.parse(code);

// console.log(ast)


const before = generate(ast, {}, code);
console.log('===before==', before)


// 遍历

traverse(ast, {
  enter(path) {
    if (babelTypes.isIdentifier(path.node, {name: 'n'})) {
      path.node.name = "x";
    }
  }
});

const after = generate(ast, {}, code);
console.log('===after==', after)





// const a = babelTypes.binaryExpression("*", babelTypes.identifier("A"), babelTypes.identifier("B"));
// console.log(a)