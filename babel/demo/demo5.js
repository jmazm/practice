// require("@babel/register")({
//   presets: ['env'],
// });

// const fs = require('fs-extra');
// const http = require('http');
// const path = require('path');
// const mdx = require('@mdx-js/mdx');
const { createCompiler } = require('@mdx-js/mdx');
// import {createCompiler} from '@mdx-js/mdx'
import { visit } from 'unist-util-visit';
import { remove } from 'unist-util-remove';


// const yaml = require('yaml');
import yaml from 'yaml'

const file = vfile(`
---
title: Hello, MDX
---
I <3 Markdown and JSX
`);

function extractFrontmatter() {
  return function transformer(tree, file) {
    visit(tree, 'yaml', function visitor(node) {
      file.data.frontmatter = yaml.parse(node.value)
    })
    remove(tree, 'yaml')
  }
}

mdxCompiler = createCompiler({
  remarkPlugins: [detectFrontmatter, extractFrontmatter]
})
mdxCompiler.process(file, function done(err, file) {
  console.log(file.data.frontmatter)
  // { title: "Hello, MDX" }
})

// var server = http.createServer(async function (request, response) {
//   const mdxText = fs.readFileSync(path.join(process.cwd(), 'index.mdx'));
//   const jsx = mdx.sync(mdxText);
//   console.log(jsx);

//   //回调数据
//   response.write(jsx);
//   response.end();
// });

// server.listen(3030);

export default {}
