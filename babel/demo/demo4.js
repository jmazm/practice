const markTwain = require('mark-twain');
const JsonML = require('jsonml.js/lib/utils');
const mdx = require('@mdx-js/mdx');
const path = require('path');

const fs = require('fs-extra');
const http = require('http');

function getCode(tree) {
  let code;
  const find = node => {
    if (code) return;
    if (!JsonML.isElement(node)) return;
    if (JsonML.getTagName(node) !== 'pre') {
      JsonML.getChildren(node).forEach(find);
      return;
    }
    code = JsonML.getChildren(JsonML.getChildren(node)[0] || '')[0] || '';
  };
  find(tree);
  return code;
}

//2. 创建http服务器
// 参数: 请求的回调, 当有人访问服务器的时候,就会自动调用回调函数
var server = http.createServer(async function (request, response) {
  console.log('有人访问了服务器');

  // markTwain
  // const json = markTwain(
  //   fs.readFileSync(path.join(process.cwd(), 'index.mdx')).toString()
  // );
  // const result = getCode(json.content);

  // @mdx-js/mdx
  // const result = await mdx(fs.readFileSync(path.join(process.cwd(), 'index.mdx')).toString())
  // console.log(result);


  

  //回调数据
  response.write(result);
  response.end();
});


server.listen(3030)