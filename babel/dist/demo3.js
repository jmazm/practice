"use strict";
exports.__esModule = true;
var markTwain = require('mark-twain');
var JsonML = require('jsonml.js/lib/utils');
function getCode(tree) {
    var code;
    var find = function (node) {
        if (code)
            return;
        if (!JsonML.isElement(node))
            return;
        if (JsonML.getTagName(node) !== 'pre') {
            JsonML.getChildren(node).forEach(find);
            return;
        }
        code = JsonML.getChildren(JsonML.getChildren(node)[0] || '')[0] || '';
    };
    find(tree);
    return code;
}
exports["default"] = (function (_a) {
    var t = _a.types;
    return {
        visitor: {
            Program: function (path) {
                var importReact = t.ImportDeclaration([t.importDefaultSpecifier(t.Identifier('React'))], t.StringLiteral('react'));
                path.unshiftContainer('body', importReact);
            },
            CallExpression: function (path) {
                if (path.node.callee.object &&
                    path.node.callee.object.name === 'ReactDOM' &&
                    path.node.callee.property.name === 'render') {
                    var app = t.VariableDeclaration('const', [
                        t.VariableDeclarator(t.Identifier('__Demo'), path.node.arguments[0])
                    ]);
                    path.scope.registerDeclaration(path.replaceWith(app)[0]);
                    var exportDefault = t.ExportDefaultDeclaration(t.Identifier('__Demo'));
                    path.insertAfter(exportDefault);
                    path.insertAfter(app);
                    path.remove();
                }
            },
            ImportDeclaration: function (path) {
                console.log(path.node);
            }
        }
    };
});
