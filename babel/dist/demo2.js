"use strict";
exports.__esModule = true;
var babelTypes = require("babel-types");
function default_1(_a) {
    var types = _a.types;
    return {
        visitor: {
            BinaryExpression: function (path) {
                console.log('22', path.node.operator, path.node);
                if (path.node.operator !== '===') {
                    return;
                }
                path.node.left = babelTypes.identifier('a1');
                path.node.right = babelTypes.identifier('b1');
            }
        }
    };
}
exports["default"] = default_1;
