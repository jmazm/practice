"use strict";
exports.__esModule = true;
var babylon = require("babylon");
var babelTypes = require("babel-types");
var babel_traverse_1 = require("babel-traverse");
var babel_generator_1 = require("babel-generator");
var babel_template_1 = require("babel-template");
var buildRequire = babel_template_1["default"]("\n  var IMPORT_NAME = require(SOURCE);\n");
var code = "n * n";
var ast = babylon.parse(code);
// console.log(ast)
var before = babel_generator_1["default"](ast, {}, code);
console.log('===before==', before);
// 遍历
babel_traverse_1["default"](ast, {
    enter: function (path) {
        if (babelTypes.isIdentifier(path.node, { name: 'n' })) {
            path.node.name = "x";
        }
    }
});
var after = babel_generator_1["default"](ast, {}, code);
console.log('===after==', after);
// const a = babelTypes.binaryExpression("*", babelTypes.identifier("A"), babelTypes.identifier("B"));
// console.log(a)
