const { program } = require('commander');

// program
//   .option('--no-sauce', 'Remove sauce')
//   .option('--cheese <flavour>', 'cheese flavour', 'mozzarella')
//   .option('--no-cheese', 'plain with no cheese')
//   .parse(process.argv);

//   console.log(program.opts())

//   const sauceStr = program.sauce ? 'sauce' : 'no-sauce';
//   const cheeseStr = program.cheese === false ? 'no cheese' : `${program.cheese} cheese`;
//   console.log(`You ordered a pizza with ${sauceStr} and ${cheeseStr}`);

// program.version('1.0.0', '-v')
// program.option('-c, --cheese [type]', 'Add cheese with optional type');
// program.parse(process.argv);
// if (program.cheese === undefined) console.log('no cheese');
// else if (program.cheese === true) console.log('add cheese');
// else console.log(`add cheese type ${program.cheese}`);

program
  .command('clone <source> [destination]')
  .alias('c')
  .description('clone a repository into a newly created directory')
  .action((source, destination) => {
    console.log('clone command called');
    console.log('source', source);
    console.log('destination', destination);
  });



  program
  .version('0.1.0')
  .command('rmdir <dir> [source] [otherDirs...]')
  .option('-r, --recursive', 'Remove recursively')
  .action(function (dir,source,  otherDirs, cmdObj) {
    console.log('dir', dir);
    console.log('source', source);
    console.log('otherDirs', otherDirs);
    console.log('cmdObj', cmdObj.recursive);
  });

// function list(val) {
//   return val.split(',');
// }


// program
//   .command("summary")
//   .alias("sm")
//   .description("Generate a `SUMMARY.md` from a folder")
//   .option("-r, --root [string]", "root folder, default is `.`")
//   .option("-t, --title [string]", "book title, default is `Your Book Title`.")
//   .option("-c, --catalog [list]", "folders to be included in book, default is `all`.")
//   .option("-i, --ignores [list]", "ignore patterns to be excluded, default is `[]`.", list)
//   .option("-u, --unchanged [list]", "unchanged catalog like `request.js`, default is `[]`.")
//   .option("-o, --outputfile [string]", "output file, default is `./SUMMARY.md`")
//   .option("-s, --sortedBy [string]", "sorted by sortedBy, for example: `num-`, default is sorted by characters")
//   .option("-d, --disableTitleFormatting", "don't convert filename/folder name to start case (for example: `JavaScript` to `Java Script`), default is `false`")

//   .action(function(options) {
//     console.error('222')
//   });


  program.parse(process.argv)