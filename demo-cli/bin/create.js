#!/usr/bin/env node

const spawn = require('cross-spawn');
const args = process.argv.slice(2);

process.exit(spawn.sync(
    'node', [require.resolve('../index.js')].concat(args), {
        stdio: 'inherit'
    }).status);